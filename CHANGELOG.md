# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.1

- patch: Documentaion update

## 0.2.0

- minor: Disabled ssl when initiating the connection which caused some issues when running the pipe

## 0.1.1

- patch: Update pipes bash toolkit version.

## 0.1.0

- minor: Initial release

